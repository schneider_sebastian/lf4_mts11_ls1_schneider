from random import randrange

class Ladung():
    def __init__(self, bezeichnung: str, menge: int) -> None:
        self.__bezeichnung: str = bezeichnung
        self.__menge: int = menge

    def get_bezeichnung(self) -> str:
        return self.__bezeichnung

    def get_menge(self) -> int:
        return self.__menge

    def set_menge(self, menge: int) -> None:
        self.__menge = menge

    def to_string(self) -> str:
        """Gibt die Attribute des Objekts als String zurück"""
        return f"Ladung: {self.__bezeichnung} ({self.__menge})"

    """
    @Override
    public static String toString() {
        return 'Ladung ' + this.bezeichung + ': 'this.menge'
    }
    """

class Raumschiff():
    __broadcast_kommunikator: list[str] = []

    def __init__(self, name: str, androiden_anzahl: int, 
            photonentorpedo_anzahl: int, energieversorgung_inp: int = 100,
            schilde_inp: int = 100, huelle_inp: int = 100,
            lebenserhaltungssysteme_inp: int = 100,
            ladungsverzeichnis: list[Ladung] = []) -> None:
        self.__name: str = name
        self.__androiden_anzahl: int = androiden_anzahl
        self.__photonentorpedo_anzahl: int = photonentorpedo_anzahl
        self.__energieversorgung_inp: int = energieversorgung_inp  # inp => in Prozent
        self.__schilde_inp: int = schilde_inp
        self.__huelle_inp: int = huelle_inp
        self.__lebenserhaltungssysteme_inp: int = lebenserhaltungssysteme_inp
        self.__ladungsverzeichnis: list[Ladung] = ladungsverzeichnis

    def zustand_ausgeben(self) -> None:
        """Gibt die Attribute des Objekts und ihre Werte 
        in der Konsole aus
        """
        print(
            f'{"Name:":<25}'
            f"{self.__name}\n"
            f'{"Androiden-Anzahl:":<25}'
            f"{self.__androiden_anzahl}\n"
            f'{"Photonentorpedo-Anzahl:":<25}'
            f"{self.__photonentorpedo_anzahl}\n"
            f'{"Energieversorgung:":<25}'
            f"{self.__energieversorgung_inp}%\n"
            f'{"Schilde:":<25}'
            f"{self.__schilde_inp}%\n"
            f'{"Hülle:":<25}'
            f"{self.__huelle_inp}%\n"
            f'{"Lebenserhaltungssysteme:":<25}'
            f"{self.__lebenserhaltungssysteme_inp}%\n" 
        )

    def ladungsverzeichnis_ausgeben(self) -> None:
        """Gibt die Attribute jedes Ladung-Objekts im Ladungsverzeichnis
        in der Konsole aus
        """
        print("Ladungsverzeichnis:")
        for ladung in self.__ladungsverzeichnis:
            print(ladung.to_string())
        print()

    def ladungsverzeichnis_aufreaumen(self) -> None:
        """Entfernt alle Ladung-Objekte aus dem Ladungsverzeichnis, 
        deren menge 0 ist
        """
        for ladung in self.__ladungsverzeichnis:
            if (ladung.get_menge() == 0):
                self.__ladungsverzeichnis.remove(ladung)

    def photonentorpedos_abschiessen(self, ziel_raumschiff) -> None:
        """Sendet eine Nachricht an alle und vermerkt einen Treffer
        beim Zielraumschiff, wenn Photonentorpedos vorhaden sind
        """
        if (self.__photonentorpedo_anzahl > 0):
            self.__photonentorpedo_anzahl -= 1
            self.nachricht_an_alle("Photonentorpedo abgeschossen")
            ziel_raumschiff.treffer_vermerken()
        else:
            self.nachricht_an_alle("-=*Click*=-")

    def phaserkanone_abschiessen(self, ziel_raumschiff) -> None:
        """Sendet eine Nachricht an alle und vermerkt einen Treffer
        beim Zielraumschiff, wenn die Energieversorgung mind. 50% ist
        """
        if (self.__energieversorgung_inp >= 50):
            self.__energieversorgung_inp -= 50
            self.nachricht_an_alle("Phaserkanone abgeschossen")
            ziel_raumschiff.treffer_vermerken()
        else:
            self.nachricht_an_alle("-=*Click*=-")

    def treffer_vermerken(self) -> None:
        """Reduziert Schilde um 50%. Wenn diese dadurch auf 0% sinken,
        wird die Hülle um 50% reduziert. Wenn diese dadurch auf 0% sinkt,
        werden die Lebenserhaltungssysteme auf 0% gesetzt und 
        eine Nachricht an alle gesendet
        """
        print(f"{self.__name} wurde getroffen!\n")
        self.__schilde_inp -= 50
        if (self.__schilde_inp < 1):
            self.__huelle_inp -= 50
            self.__energieversorgung_inp -= 50
            if (self.__huelle_inp < 1):
                self.__lebenserhaltungssysteme_inp = 0
                self.nachricht_an_alle(
                    "Die Lebenserhaltungssysteme wurden vernichtet"
                )

    def nachricht_an_alle(self, nachricht: str) -> None:
        """Speichert nachricht im broadcast_kommunikator"""
        Raumschiff.__broadcast_kommunikator.append(
            f"{self.__name}: {nachricht}"
        )

    def logbuch_eintraege_zurueckgeben() -> None:
        """Gibt die Inhalte von broadcast_kommunikator 
        in der Konsole aus
        """
        for nachricht in Raumschiff.__broadcast_kommunikator:
            print(nachricht)

    def photonentorpedos_aufladen(self, anzahl: int) -> None:
        """Erhöht photonentorpedo_anzahl um anzahl, wenn die Menge 
        der Ladung mit Bezeichnung "Photonentorpedo" ausreicht,
        ansonsten gegebene Menge. Falls es keine Ladung mit 
        dieser Bezeichung im Ladungsverzeichnis gibt, 
        bleibt die photonentorpedo_anzahl unverändert und 
        es wird eine Nachricht an alle gesendet
        """
        found = False
        for ladung in self.__ladungsverzeichnis:
            if (ladung.get_bezeichnung() == "Photonentorpedo"):
                found = True
                if (anzahl > ladung.get_menge()):
                    self.__photonentorpedo_anzahl += ladung.get_menge()
                    ladung.set_menge(0)
                else:
                    self.__photonentorpedo_anzahl += anzahl
                    ladung.set_menge(ladung.get_menge() - anzahl)
                break
        if not found:
            print("Keine Photonentorpedos gefunden!\n")
            self.nachricht_an_alle("-=*Click*=-")  # find ich unsinnig

    def reperatur_androiden_einsetzen(self, androiden_anzahl: int, 
            schilde: bool = False, huelle: bool = False, 
            lebenserhaltungssysteme: bool = False,
            energieversorgung: bool = False) -> None:
        """Der Wert, aus der Multiplikation einer Zufallszahl (zwischen 0 
        und 100) mit der Anzahl der Androiden, wir gleichmäßig verteilt 
        auf die angegebenen Schiffsstrukturen addiert"""
        zu_reparierende_schiffsstrukturen = (
            schilde + huelle + lebenserhaltungssysteme + energieversorgung
        )
        if (androiden_anzahl > self.__androiden_anzahl):
            androiden_anzahl = self.__androiden_anzahl
        reperatur_inp = int(
            (randrange(101) * androiden_anzahl) 
            / zu_reparierende_schiffsstrukturen // 1
        )
        if schilde:
            self.__schilde_inp += reperatur_inp
        if huelle:
            self.__huelle_inp += reperatur_inp
        if lebenserhaltungssysteme:
            self.__lebenserhaltungssysteme_inp += reperatur_inp
        if energieversorgung:
            self.__energieversorgung_inp += reperatur_inp


def main():
    x1 = Ladung("Ferengi Schneckensaft", 200)
    x2 = Ladung("Bat'leth Klingonen Schwert", 200)
    x3 = Ladung("Borg-Schrott", 5)
    x4 = Ladung("Rote Materie", 2)
    x5 = Ladung("Plasma-Waffe", 50)
    x6 = Ladung("Forschungssonde", 35)
    x7 = Ladung("Photonentorpedo", 2)

    klingonen = Raumschiff("IKS Hegh'ta", 2, 3, ladungsverzeichnis=[x1, x2])
    romulaner = Raumschiff("IRW Khazara", 2, 2, schilde_inp=200, ladungsverzeichnis=[x3, x4, x5])
    vulkanier = Raumschiff("Ni'Var", 5, 1, ladungsverzeichnis=[x6, x7])

    klingonen.photonentorpedos_abschiessen(romulaner)
    romulaner.phaserkanone_abschiessen(klingonen)
    vulkanier.nachricht_an_alle("Gewalt ist nicht logisch")
    klingonen.zustand_ausgeben()
    klingonen.ladungsverzeichnis_ausgeben()
    vulkanier.reperatur_androiden_einsetzen(6, True, True, False, True)
    vulkanier.photonentorpedos_aufladen(2)
    vulkanier.ladungsverzeichnis_aufreaumen()
    klingonen.photonentorpedos_abschiessen(romulaner)
    klingonen.photonentorpedos_abschiessen(romulaner)
    klingonen.zustand_ausgeben()
    klingonen.ladungsverzeichnis_ausgeben()
    romulaner.zustand_ausgeben()
    romulaner.ladungsverzeichnis_ausgeben()
    vulkanier.zustand_ausgeben()
    vulkanier.ladungsverzeichnis_ausgeben()
    Raumschiff.logbuch_eintraege_zurueckgeben()

main()