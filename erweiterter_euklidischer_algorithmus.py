

from typing import Any


def euklidischer_algorithmus(a: int, b: int) -> tuple[int, list[dict]]:
    rechenverlauf = []
    r = None
    while (r != 0):
        q = a//b
        r = a - b*q
        rechenverlauf.append({"a": a, "b": b, "q": q, "r": r})
        a = b
        b = r

    result = ()
    if (len(rechenverlauf) == 1):
        if abs(a) < abs(b):
            result = (abs(a), rechenverlauf)
        else:
            result = (abs(b), rechenverlauf)
    else:
        result = (rechenverlauf[-2]["r"], rechenverlauf)

    return result


def erweiterter_euklidischer_algorithmus(a: int, b: int) -> dict:
    ggt, rechenverlauf = euklidischer_algorithmus(a, b)

    rechenverlauf[-1].update({"x": 0, "y": 1})
    for i in range(len(rechenverlauf)-2, -1, -1):
        rechenverlauf[i].update({
            "x": rechenverlauf[i+1]["y"],
            "y": rechenverlauf[i+1]["x"] - rechenverlauf[i]["q"]*rechenverlauf[i+1]["y"]
        })

    result = {
        "ggt": ggt, 
        "a": rechenverlauf[0]["a"], 
        "x": rechenverlauf[0]["x"],
        "b": rechenverlauf[0]["b"], 
        "y": rechenverlauf[0]["y"]
    }

    return result


def inverse_von_modulo(a: int, b: int) -> int:
    """result = 'b'**-1 mod 'a'
    """
    result = None
    gleichung = erweiterter_euklidischer_algorithmus(a, b)
    if (gleichung["ggt"] == 1):
        result = gleichung["y"]
        while (result < 0):
            result += gleichung["a"]

    return result

a = 48
b = 14
ea = euklidischer_algorithmus(a, b)
print(ea)
eea = erweiterter_euklidischer_algorithmus(a, b)
print(f"{eea['ggt']} = {eea['a']}*{eea['x']} + {eea['b']}*{eea['y']}")
im = inverse_von_modulo(a, b)
print(f"{im} = {b}**-1 mod {a}")
