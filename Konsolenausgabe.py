#-----------------Aufgabe 1--------------------------------

print("Das ist ein Beispielsatz. Ein Beispielsatz ist das.")
print()
print("Das ist ein \"Beispielsatz\".\nEin Beispielsatz ist das.")

print()#----------Aufgabe 2--------------------------------

for i in range(1,14,2):
    print(" "*((13-i)//2),end='')
    print("*"*i)
print(" "*5 + "***")
print(" "*5 + "***")

print()#----------Aufgabe 2 (v2)---------------------------

print('%7s' % '*')
print('%8s' % ('*'*3))
print('%9s' % ('*'*5))
print('%10s' % ('*'*7))
print('%11s' % ('*'*9))
print('%12s' % ('*'*11))
print('%13s' % ('*'*13))
print('%8s' % '***')
print('%8s' % '***')

print()#----------Aufgabe 3--------------------------------

a = 22.4234234
b = 111.2222
c = 4.0
d = 1000000.551
e = 97.34

arr = [a,b,c,d,e]
for e in arr:
    print("%.2f" % e)
