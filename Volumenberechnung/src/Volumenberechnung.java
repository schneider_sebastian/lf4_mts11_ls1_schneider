import java.util.Scanner;
import java.lang.Math;

public class Volumenberechnung {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Dieses Programm berechnet Volumen");
		
		System.out.println("W�rfel:");		
		System.out.print("a = ");
		float a = scan.nextFloat();
		System.out.printf("V = %f\n\n", wurfel(a));
		
		System.out.println("Quader:");		
		System.out.print("a = ");
		a = scan.nextFloat();
		System.out.print("b = ");
		float b = scan.nextFloat();
		System.out.print("c = ");
		float c = scan.nextFloat();
		System.out.printf("V = %f\n\n", quader(a,b,c));
		
		System.out.println("Pyramide:");		
		System.out.print("a = ");
		a = scan.nextFloat();
		System.out.print("h = ");
		float h = scan.nextFloat();
		System.out.printf("V = %f\n\n", pyramide(a,h));
		
		System.out.println("Kugel:");		
		System.out.print("r = ");
		float r = scan.nextFloat();
		System.out.printf("V = %f\n\n", kugel(r));
	}
	
	public static float wurfel(float a) {
		return a*a*a;
	}
	
	public static float quader(float a, float b, float c) {
		return a*b*c;
	}
	
	public static float pyramide(float a, float h) {
		return a*a*h/3;
	}
	
	public static float kugel(float r) {
		return (float) (4/3 * Math.pow(r,3) * Math.PI);
	}
}

