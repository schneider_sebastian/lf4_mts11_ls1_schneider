import java.util.Scanner;

public class Multiplikation {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("This program multiplies a and b");
		
		System.out.print("a = ");
		float a = scan.nextFloat();
		System.out.print("b = ");
		float b = scan.nextFloat();
		
		System.out.printf("%f * %f = %f", a, b, multiply(a,b));
	}
	
	public static float multiply(float a, float b) {
		return a*b;
	}
}
