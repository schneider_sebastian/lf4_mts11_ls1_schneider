#A8: Quadrat
seitenlaenge = int(input(
    "Geben Sie die Seitenlänge ihrer Wunschquadrates ein! >> "
))

for i in range(seitenlaenge):
    if (i == 0) or (i == seitenlaenge-1):
        print("* " * seitenlaenge)
    else:
        print("* ", "  "*(seitenlaenge-2), "*", sep="")

print()#----------------

#A9: Treppe
print("Beschreiben Sie ihre Wunschtreppe!")
hoehe = int(input("Treppehöhe\t>> "))
breite = int(input("Stufenbreite\t>> "))

for i in range(hoehe):
    print(("*"*breite*(i+1)).rjust(hoehe*breite," "))


#A8: Matrix
def digitAtReverseIndex(number:int, i:int):
    return int(str(number)[::-1][i])

def quersumme(number:int):
    summe = 0
    for i in range(len(str(number))):
        summe += digitAtReverseIndex(number, i)
    return summe / len(str(number))

def hasDigit(number:int, digit:int):
    result = False
    for i in range(len(str(number))):
        if digitAtReverseIndex(number, i) == digit:
            result = True
    return result

zahl = int(input("Geben Sie eine Zahl zwischen inkl. 2 und 9 ein! >> "))

if (zahl >=2) and (zahl <= 9):
    count = 0
    while (count < 100):
        if ((count % zahl == 0) and (count != 0)) or (hasDigit(count, zahl)) or (quersumme(count) == zahl):
            print("  *", end="")
        else:
            print(str(count).rjust(3," "),end="")

        if ((count+1) % 10 == 0):
            print()
        
        count += 1