from sympy import symbols, simplify
from re import finditer

SYM_X = symbols("x")


def interpolate(points: list[tuple[float, float]]):
    base_functions = []
    for x, y in points:
        base_function = 1
        for xj, yj in points:
            if (xj, yj) != (x, y):
                base_function *= (SYM_X - xj) / (x - xj)
        base_functions.append(base_function)

    function = 0
    for base_function, (x, y) in zip(base_functions, points):
        function += base_function*y

    return simplify(function)


def main():
    amount_of_points = int(input("amount of points >> "))

    print("point input example: '-1 3.3'")
    points = []
    for i in range(amount_of_points):
        text = input(f"point {i}\t>> ")
        # get all numbers on the line
        input_numbers = [number.group() for number in finditer(
            r"[-+]?([0-9]*[.]?)[0-9]+", text
        )]
        # define a point with the first two numbers on the line
        points.append((int(input_numbers[0]), int(input_numbers[1])))

    function = interpolate(points)
    print("f(x) =", function)


if __name__ == "__main__":
    main()
